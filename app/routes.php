<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/


Route::get('/admin', function() {
    return Redirect::to('https://crm.prisu.nz/');
});

Route::get('/', array(
    'as' => 'home',
    'uses' => 'HomeController@getHome'
));

Route::get('/carpets', array(
    'as' => 'carpets',
    'uses' => 'HomeController@getCarpets'
));

Route::get('/hard-flooring', array(
    'as' => 'hard',
    'uses' => 'HomeController@getHard'
));

Route::get('/free-measure-and-quote', array(
    'as' => 'free-measure',
    'uses' => 'HomeController@getFreeMeasure'
));

Route::get('price-calculator', array(
    'as' => 'price-cal',
    'uses' => 'HomeController@getPriceCal'
));

Route::get('/blog', array(
    'as' => 'blog',
    'uses' => 'HomeController@getBlog'
));

Route::get('/contact', array(
    'as' => 'contact',
    'uses' => 'HomeController@getContact'
));

Route::get('/carpets/category/{categoryId}', array(
    'as' => 'carpet-category',
    'uses' => 'HomeController@getCarpetCategory'
));

Route::get('/hard-flooring/category/{categoryId}', array(
    'as' => 'carpet-category',
    'uses' => 'HomeController@getHardCategories'
));

Route::get('/product/{productId}', array(
    'as' => 'carpet-product',
    'uses' => 'HomeController@getCarpetProduct'
));

App::missing(function($exception) {
    $sliderIds = [25, 26, 36];
    //API Url
    $url = 'https://crm.prisu.nz/website/getsliders';
    $ch = curl_init($url);

    //The JSON data.

    $jsonData = array(
        'orgkey' => 'Z9SG0006',
        'orgid' => 6,
        'sliderids' => $sliderIds
    );

    //Encode the array into JSON.
    $jsonDataEncoded = json_encode($jsonData);

    //Tell cURL that we want to send a POST request.
    curl_setopt($ch, CURLOPT_POST, 1);

    //Attach our encoded JSON string to the POST fields.
    curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);

    //Set the content type to application/json
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));

    //Execute the request
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $result = curl_exec($ch);
    $result = json_decode($result, true);

    return View::make('404', array(
        'page' => '404',
        'masterObject' => $result,
        'orgId' => 6
    ));
});
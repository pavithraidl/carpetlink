<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 2/02/18 08:59.
 */
?>

@extends('master')


@section('content')

    <div class="container" id="price-cal-container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <h3>{{ $pageObject['34']['51']['content_title'] }}</h3>
            </div>
        </div>
        <div class="row">
            <?php
                if($pageObject['34']['51']['calculator_left_image'] == 1) {
                    $imgUrl = 'https://crm.prisu.nz/assets/backend/images/website/slide-block/'.$orgId.'/'.$pageObject['34']['51']['calculator_left_image_id'].'.jpg';
                }
                else {
                    $imgUrl = 'plese set to default';
                }
            ?>
            <div class="col-md-10 col-md-offset-1">
                <div class="col-md-12 form-area">
                    <div class="col-md-5 cal-img hidden-sm hidden-xs">
                        <img src="{{ $imgUrl }}" />
                    </div>
                    <div class="col-md-7 input-container">
                        {{--<div class="input-section">--}}
                            {{--<h4>1. Carpet or Hard Flooring</h4>--}}
                            {{--<div class="col-md-12">--}}
                                {{--<div class="form-group col-md-6">--}}
                                    {{--<input id="rdo-carpet" onchange="selectCarpetHard();" checked value="1" name="carpethard" type="radio" />--}}
                                    {{--<label for="rdo-carpet">CARPET</label>--}}
                                {{--</div>--}}
                                {{--<div class="form-group col-md-6">--}}
                                    {{--<input id="rdo-hard" name="carpethard" onchange="selectCarpetHard();" value="2" type="radio" />--}}
                                    {{--<label for="rdo-hard">HARD FLOORING</label>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        <div class="input-section">
                            <h4 id="drop-box-title">1. Start Typing Carpet/Hard Floor Name</h4>
                            <input type="text" class="form-control" id="keyword" />
                            <div id="search-result-container" class="search-result">
                                <ul id="search-result-ul">
                                    {{--JQuery append--}}
                                </ul>
                                <p style="text-align: center";>
                                    <img id="search-loading-icon" src="https://crm.prisu.nz/assets/backend/images/loading/ring-sm.svg" />
                                </p>

                            </div>
                        </div>
                        <input type="hidden" id="price" value="" />
                        <div class="input-section">
                            <h4>2. Enter Room Dimensions</h4>
                            <div class="col-md-12">
                                <div class="form-group col-md-4 col-sm-6">
                                    <label for="input-length">LENGTH (m)</label>
                                    <input id="input-length" onchange="calculatePrice();" class="length only-digits" maxlength="3" max="999" min="0" onkeydown="maxVal(this, 999)" type="number" value="" />
                                    <span class="hidden-xs hidden-sm">m</span>
                                </div>
                                <div class="form-group col-md-4 col-sm-6">
                                    <label for="input-long">LENGTH (m)</label>
                                    <input id="input-long" onchange="calculatePrice();" class="length only-digits" maxlength="3" max="999" min="0" onkeydown="maxVal(this, 999)" type="number" value="" />
                                    <span class="hidden-xs hidden-sm">m</span>
                                </div>
                                <div class="form-group col-md-4 total-area-group">
                                    <label for="input-length">TOTAL AREA (sq m)</label>
                                    <h4 class="total-area"><display id="total-area-display" style="text-align: left; margin-left: -20px;"> 0.00</display></h4><span class="hidden-sm hidden-xs">sq. m</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="cost-display"><p>Total Cost: <strong id="cost">$ 0</strong></p></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-10 col-md-offset-1">
                {{ $pageObject['34']['51']['content'] }}
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    {{HTML::script('/assets/js/calculator.js')}}
@endsection
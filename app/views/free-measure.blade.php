<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 2/02/18 08:59.
 */
?>

@extends('master')

@section('content')

    <?php
        if($pageObject['33']['50']['top_banner_image'] == 1) {
            $imgUrl = 'https://crm.prisu.nz/assets/backend/images/website/slide-block/'.$orgId.'/'.$pageObject['33']['50']['top_banner_image_id'].'.jpg';
        }
        else {
            $imgUrl = 'plese set to default';
        }
    ?>

    <div class="row category-banner">
        <img src="{{ $imgUrl }}" />
    </div>

    <div class="container free-measure">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <h5>{{ $pageObject['33']['50']['content_title'] }}</h5>
                {{ $pageObject['33']['50']['content'] }}
            </div>
        </div>
    </div>

    <div class="contact-form">
        <div class="container">
            <h5 class="free-measure-h5">{{ $pageObject['33']['50']['form_title'] }}</h5>
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="col-md-4 form-group">
                        <input type="text" class="form-control" placeholder="Name" />
                    </div>
                    <div class="col-md-4 form-group">
                        <input type="text" class="form-control" placeholder="Name" />
                    </div>
                    <div class="col-md-4 form-group">
                        <input type="text" class="form-control" placeholder="Name" />
                    </div>
                    <div class="col-md-12 form-group">
                        <textarea class="form-control" placeholder="Message"></textarea>
                    </div>
                    <p>
                        <button>Submit</button>
                    </p>
                </div>
            </div>
        </div>
    </div>

@endsection

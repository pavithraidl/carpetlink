<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 2/02/18 09:50.
 */
?>

@extends('master')

@section('content')

    <input type="hidden" id="category-id" value="{{ $category['id'] }}" />
    <div class="container clearfix">

        <div class="single-product">
            <div class="product">
                {{--<div class="col-md-4">--}}

                    {{--@include('includes.side-filters')--}}

                {{--</div>--}}

                <div class="col-md-10 col-md-offset-1 product-desc">
                    <!-- Product Single - Price
                    ============================================= -->
                    <div class="product-page-title">{{ $category['name'] }}</div><!-- Product Single - Price End -->
                    <p class="product-page-title-p">
                        {{ $category['description'] }}
                    </p>
                    <?php
                        if($category['img'] == 1) {
                            $imgUrl = 'https://crm.prisu.nz/assets/backend/images/products/categories/'.$orgId.'/'.$category['id'].'.jpg';
                        }
                        else {
                            $imgUrl = 'please set to default';
                        }

                        if($category['banner'] == 1) {
                            $bannerImgUrl = 'https://crm.prisu.nz/assets/backend/images/products/categories/'.$orgId.'/'.$category['id'].'-banner.jpg';
                        }
                        else {
                            $bannerImgUrl = URL::To('/').'/assets/img/slider/super-soft-eco-plus@2x.jpg';
                        }
                    ?>
                    <img class="all-products-banner" src="{{$bannerImgUrl}}" />
                    <div class="clear"></div>
                    <div class="line"></div>

                    <!-- Product Single - Quantity & Cart Button
                    ============================================= -->
                    <div class="col-md-12 all-products-products">
                        {{--<div class="product-filter pull-right">--}}
                            {{--<label>Sort By:</label>--}}
                            {{--<select>--}}
                                {{--<option>BY POPULARITY</option>--}}
                            {{--</select>--}}
                        {{--</div>--}}
                        <div id="product-list-container">
                            <p style="text-align: center;">
                                <img src="{{URL::To('/')}}/assets/img/loading/loader.gif" />
                            </p>
                            {{--JQuery append--}}
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="clear"></div><div class="line"></div>
    </div>

@endsection

@section('scripts')
    {{HTML::script('assets/js/all-products.js')}}
@endsection

<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 2/02/18 08:58.
 */
?>

@extends('master')

@section('content')
    <?php
        if($pageObject['30']['47']['top_banner_image'] == 1) {
            $imgUrl = 'https://crm.prisu.nz/assets/backend/images/website/slide-block/'.$orgId.'/'.$pageObject['30']['47']['top_banner_image_id'].'.jpg';
        }
        else {
            $imgUrl = 'plese set to default';
        }
    ?>
    <div class="row category-banner">
        <img src="{{ $imgUrl }}" />
    </div>
    <div class="container clearfix">

        <div class="single-product">
            <div class="product">
                <div class="col-md-10 col-md-offset-1 product-desc">

                    <!-- Product Single - Quantity & Cart Button
                    ============================================= -->
                    <div class="col-md-12 all-products-products">
                        <div class="row">
                            <div id="carpet-category-container" class="col-md-12">
                                <p style="text-align: center;">
                                    <img src="{{URL::To('/')}}/assets/img/loading/loader.gif" />
                                </p>
                                {{--JQuery append--}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="clear"></div><div class="line"></div>
    </div>

@endsection


@section('scripts')
    {{HTML::script('assets/js/carpets.js')}}
@endsection

<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 1/02/18 22:57.
 */
?>
        <!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>

    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="author" content="Prisu" />

    <!-- Stylesheets
    ============================================= -->
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />
    {{HTML::style('/assets/css/bootstrap.css')}}
    {{HTML::style('/assets/style.css')}}
    {{HTML::style('/assets/css/swiper.css')}}
    {{HTML::style('/assets/css/dark.css')}}
    {{HTML::style('/assets/css/font-icons.css')}}
    {{HTML::style('/assets/css/animate.css')}}
    {{HTML::style('/assets/css/magnific-popup.css')}}
    {{HTML::style('/assets/libs/font-awesome-4.7.0/css/font-awesome.min.css')}}
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">

    {{HTML::style('/assets/css/responsive.css')}}
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <!-- Document Title
    ============================================= -->
    <title>Home - Carpet Link</title>
    <link rel="icon" href="{{URL::To('/')}}/assets/img/fav-icon.png">

</head>

<body class="stretched">
<?php
    $variables = new VariableController();
?>
<input type="hidden" id="org-id" value="{{ $variables->getVariable('orgid') }}" />
<input type="hidden" id="org-key" value="{{ $variables->getVariable('key') }}" />
<input type="hidden" id="org-secret" value="{{ $variables->getVariable('secret') }}" />
<input type="hidden" id="org-server" value="{{ $variables->getVariable('server') }}" />
<input type="hidden" id="org-domain" value="{{ $variables->getVariable('domain') }}" />

<!-- Document Wrapper
============================================= -->
<div id="wrapper" class="clearfix">

    <!-- Header
    ============================================= -->
    <header id="header" class="sticky-style-2">

        <div class="container clearfix">

            <!-- Logo
            ============================================= -->
            <div id="logo">
                <a href="{{URL::To('/')}}" class="standard-logo" data-dark-logo="{{URL::To('/')}}/assets/img/logo.jpg"><img class="divcenter" src="{{URL::To('/')}}/assets/img/logo.jpg" alt="Carpetlink Logo"></a>
                <a href="{{URL::To('/')}}" class="retina-logo" data-dark-logo="{{URL::To('/')}}/assets/img/logo@2x.jpg"><img class="divcenter" src="{{URL::To('/')}}/assets/img/logo@2x.jpg" alt="Carpetlink Logo"></a>
            </div><!-- #logo end -->
            <div id="header-phone">
                <a href="tel:{{ $masterObject[25][42]['phone_number'] }}"><img src="{{URL::To('/')}}/assets/img/icons/phone-icon.svg" />{{ $masterObject[25][42]['phone_number'] }}</a>
            </div>

        </div>

        <div id="header-wrap">

            <!-- Primary Navigation
            ============================================= -->
            <nav id="primary-menu" class="style-2 center">

                <div class="container clearfix">

                    <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>

                    <ul>
                        <li @if($page == 'home') class="current" @endif><a href="{{URL::Route('home')}}"><div><strong class="hidden-md hidden-lg">Home</strong><img class="hidden-sm hidden-xs" src="{{URL::To('/')}}/assets/img/icons/home.svg" /> </div></a></li>
                        <li @if($page == 'carpets') class="current" @endif><a href="{{URL::Route('carpets')}}"><div>Carpets</div></a></li>
                        <li @if($page == 'hard') class="current" @endif><a href="{{URL::Route('hard')}}"><div>Hard Flooring</div></a></li>
                        <li @if($page == 'free-measure') class="current" @endif><a href="{{URL::Route('free-measure')}}"><div>Free Measure and Quote</div></a></li>
                        <li @if($page == 'price-cal') class="current" @endif><a href="{{URL::Route('price-cal')}}"><div>Price Calculator</div></a></li>
                        <li @if($page == 'blog') class="current" @endif><a href="{{URL::Route('blog')}}"><div>Blog</div></a></li>
                        <li @if($page == 'contact') class="current" @endif><a href="{{URL::Route('contact')}}"><div>Contact</div></a></li>
                    </ul>

                    <!-- Top Search
                    ============================================= -->
                    <div id="top-search">
                        <a href="#" id="top-search-trigger"><i class="icon-search3"></i><i class="icon-line-cross"></i></a>
                        <form id="search-form">
                            <input id="search-keyword" type="text" class="form-control" value="" placeholder="Start Typing...">
                            <div class="search-dialog">
                                <h4 class="title">Search Results</h4>
                                <div class="product-list row">
                                    <div id="product-search-result-container" class="col-md-12">
                                        {{--JQuery append--}}
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div><!-- #top-search end -->

                </div>

            </nav><!-- #primary-menu end -->

        </div>

    </header><!-- #header end -->

    @if($page == 'home')
    <section id="slider" class="slider-parallax swiper_wrapper clearfix">

        <div class="swiper-container swiper-parent">
            <div class="swiper-wrapper">
                @foreach($masterObject['26'] as $slide)
                <?php
                    if($slide['slider_image'] == 1) {
                        $imgUrl = 'https://crm.prisu.nz/assets/backend/images/website/slide-block/'.$orgId.'/'.$slide['slider_image_id'].'.jpg';
                    }
                    else {
                        $imgUrl = 'plese set to default';
                    }
                ?>
                <div class="swiper-slide dark" style="background-image: url('{{ $imgUrl }}');">
                    <div class="row slider-shade-container">
                        <div class="slider-shade-content">
                            <div class="col-md-12">
                                {{ $slide['slider_content'] }}
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            <div id="slider-arrow-left"><i class="icon-angle-left"></i></div>
            <div id="slider-arrow-right"><i class="icon-angle-right"></i></div>
            <div id="slide-number"><div id="slide-number-current"></div><span>/</span><div id="slide-number-total"></div></div>
        </div>
    </section>
    @endif

    <!-- Content
    ============================================= -->
    <section id="content" @if($page == 'carpets' || $page == 'hard') class="all-products" @endif>
        <div class="content-wrap">
            <div class="mobile-call-button">
                <a href="tel:{{ $masterObject[25][42]['phone_number'] }}"> <i class="fa fa-phone-square"></i></a>
            </div>
            {{--Content--}}
                @yield('content')
            {{--!end content--}}
            <div class="section nobottommargin section-5">
                <div class="container">
                    <div class="col-md-12">
                        <div class="col-md-6 flooring">
                            <img src="{{URL::To('/')}}/assets/img/home/group-9@3x.png"/>
                        </div>
                        <div class="col-md-6">
                            <p class="follow-us">
                                Follow us on:
                                <a href="#"><img src="{{URL::To('/')}}/assets/img/home/linkedin.png" /> </a>
                                <a href="#"><img src="{{URL::To('/')}}/assets/img/home/yelp.png" /> </a>
                                <a href="#"><img src="{{URL::To('/')}}/assets/img/home/loc.png" /> </a>
                                <a href="#"><img src="{{URL::To('/')}}/assets/img/home/facebook.png" /> </a>
                            </p>
                        </div>
                    </div>
                    <div class="col-md-12 footer-dealers">
                        @foreach($masterObject['36'] as $contact)
                        <div class="col-md-3 col-sm-6">
                            <h5>{{ $contact['store_location'] }}</h5>
                            <ul>
                                <li><i class="fa fa-minus"></i> Store Manager:{{ $contact['store_manager_name'] }}</li>
                                <li><i class="fa fa-minus"></i> <a href="tel:{{ $contact['store_manager_phone_number'] }}">Phone: {{ $contact['store_manager_phone_number'] }}</a> </li>
                                <li><i class="fa fa-minus"></i> <a href="mailto:{{ $contact['store_manager_email'] }}">{{ $contact['store_manager_email'] }}</a></li>
                            </ul>
                            <h5>Open Hours</h5>
                            {{ $contact['open_hours'] }}
                        </div>
                        @endforeach
                        <div class="col-md-3 col-sm-6 q-card">
                            <h5>Click here to apply for pre-approved finance</h5>
                            <p>
                                <img src="{{URL::To('/')}}/assets/img/home/q-card@2x.png" />
                            </p>
                            <p>
                                Q Card leading creteria, fees, terms and conditions apply
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- #content end -->
    <!-- Footer
    ============================================= -->
    <footer id="footer" class="dark">
        <!-- Copyrights
        ============================================= -->
        <div id="copyrights">

            <div class="container clearfix">
                <div class="col-md-10 col-md-offset-1 col-xs-12">
                    <ul class="footer-menu-links">
                        <li><a href="{{URL::Route('home')}}">Home</a> </li>
                        <li><a href="{{URL::Route('carpets')}}">Carpets</a></li>
                        <li><a href="{{URL::Route('hard')}}">Hard Flooring</a></li>
                        <li class="hidden-xs"><a href="{{URL::Route('free-measure')}}">Free Measure and Quote</a></li>
                        <li><a href="{{URL::Route('price-cal')}}">Price Calculator</a></li>
                        <li><a href="{{URL::Route('blog')}}">Blog</a></li>
                        <li><a href="{{URL::Route('contact')}}">Contact</a></li>
                    </ul>
                </div>

            </div>

        </div><!-- #copyrights end -->

    </footer><!-- #footer end -->

</div><!-- #wrapper end -->

<!-- Go To Top
============================================= -->
<div id="gotoTop" class="icon-angle-up"></div>

<!-- External JavaScripts
============================================= -->
{{HTML::script('assets/js/jquery.js')}}
{{HTML::script('assets/js/plugins.js')}}
{{HTML::script('assets/js/jqueryui.min.js')}}

<!-- Footer Scripts
============================================= -->
{{HTML::script('assets/js/functions.js')}}
{{HTML::script('assets/js/constant.js')}}
{{HTML::script('assets/js/public.functions.js')}}

@yield('scripts')

</body>
</html>
<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 2/02/18 09:38.
 */
?>

<div class="filter-dialog-container">
    <div class="filter-dialog">
        <h3>Filter By</h3>
        <div class="clear"></div>
        <div class="line" style="margin-bottom: 20px"></div>
        <div class="area">
            <h4 id="color-title">COLOR <i class="fa  fa-minus-square-o pull-right"></i> </h4>
            <div id="color-container" class="row">
                <div class="col-md-12">
                    <div class="col-md-4">
                        <div class="color"></div>
                    </div>
                    <div class="col-md-4">
                        <div class="color"></div>
                    </div>
                    <div class="col-md-4">
                        <div class="color"></div>
                    </div>
                    <div class="col-md-4">
                        <div class="color"></div>
                    </div>
                    <div class="col-md-4">
                        <div class="color"></div>
                    </div>
                    <div class="col-md-4">
                        <div class="color"></div>
                    </div>
                </div>
            </div>
            <div class="line"></div>
        </div>
        <div class="area">
            <h4 id="price-title">PRICE (per sq m) <i class="fa  fa-minus-square-o pull-right"></i> </h4>
            <div id="price-container" class="row">
                <div class="col-md-12">
                                                <span style="position: relative;"> <icon>$</icon>
                                                    <input type="number" value="" max="9999" maxlength="4" min="0" />
                                                </span> -
                    <span style="position: relative;"> <icon>$</icon>
                                                    <input type="number" value="" max="9999" maxlength="4" min="1" />
                                                </span>
                    <button class="btn btn-default">Go</button>
                </div>
            </div>
            <div class="line"></div>
        </div>
        <div class="area">
            <h4 id="range-title">RANGE <i class="fa fa-minus-square-o pull-right"></i></h4>
            <div id="range-container" class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <input type="checkbox" class="chk" />
                        <label>Super Soft Eco Plus</label>
                    </div>
                    <div class="form-group">
                        <input type="checkbox" class="chk" />
                        <label>Solutions Dyed Nylon</label>
                    </div>
                </div>
            </div>

            <div class="line"></div>
        </div>
        <div class="area">
            <h4 id="features-title">FEATURES <i class="fa fa-minus-square-o pull-right"></i></h4>
            <div id="features-container" class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <input type="checkbox" class="chk" />
                        <label>Feature 1</label>
                    </div>
                    <div class="form-group">
                        <input type="checkbox" class="chk" />
                        <label>Feature 2</label>
                    </div>
                    <div class="form-group">
                        <input type="checkbox" class="chk" />
                        <label>Feature 3</label>
                    </div>
                    <div class="form-group">
                        <input type="checkbox" class="chk" />
                        <label>Feature 4</label>
                    </div>
                </div>
            </div>
            <div class="line"></div>
        </div>
        <div class="area">
            <h4 id="material-title">MATERIAL <i class="fa fa-minus-square-o pull-right"></i></h4>
            <div id="material-container" class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <input type="checkbox" class="chk" />
                        <label>Material 1</label>
                    </div>
                    <div class="form-group">
                        <input type="checkbox" class="chk" />
                        <label>Material 2</label>
                    </div>
                </div>
            </div>
            <div class="line"></div>
        </div>
        <div class="area">
            <h4 id="style-title">STYLE <i class="fa fa-minus-square-o pull-right"></i></h4>
            <div id="style-container" class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <input type="checkbox" class="chk" />
                        <label>Style 2</label>
                    </div>
                    <div class="form-group">
                        <input type="checkbox" class="chk" />
                        <label>Style 2</label>
                    </div>
                </div>
            </div>
            <div class="line"></div>
        </div>
    </div>
</div>

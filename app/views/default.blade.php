<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 2/02/18 08:57.
 */
?>

@extends('master')

@section('content')

    <div class="container clearfix">
        <div class="row topmargin-lg bottommargin-lg">
            <div class="col-md-12 section-1">
                {{ $pageObject[27][44]['content'] }}
            </div>
        </div>
    </div>
    <div class="row clearfix section-2-container">
        <div class="col-md-12 center section-2">
            <?php
                if($pageObject['28']['45']['left_image'] == 1) {
                    $leftImgUrl = 'https://crm.prisu.nz/assets/backend/images/website/slide-block/'.$orgId.'/'.$pageObject['28']['45']['left_image_id'].'.'.$pageObject['28']['45']['left_image_ext'];
                }
                else {
                    $leftImgUrl = 'plese set to default';
                }

                if($pageObject['28']['45']['right_image'] == 1) {
                    $rightImgUrl = 'https://crm.prisu.nz/assets/backend/images/website/slide-block/'.$orgId.'/'.$pageObject['28']['45']['right_image_id'].'.'.$pageObject['28']['45']['right_image_ext'];
                }
                else {
                    $rightImgUrl = 'plese set to default';
                }
            ?>
            <div class="col-md-6 carpet"  style="background: url('{{ $leftImgUrl }}') center center no-repeat; background-size: cover;">
                <a href="{{URL::Route('carpets')}}">
                    <div class="red-block">
                        <h4>Carpets</h4>
                        <p>EXPLORE THE FULL RANGE ></p>
                    </div>
                </a>

            </div>
            <div class="col-md-6 hard-floor" style="background: url('{{ $rightImgUrl }}') center center no-repeat; background-size: cover;">
                <a href="{{URL::Route('hard')}}">
                    <div class="red-block">
                        <h4>Hard Flooring</h4>
                        <p>EXPLORE THE FULL RANGE ></p>
                    </div>
                </a>

            </div>
        </div>
    </div>
    <div class="container nobottommargin">
        <div class="section-3">
            <div class="col-md-12 col-xs-12">
                {{ $pageObject['29']['46']['content'] }}
                <div class="form">
                    <div class="col-md-4 col-sm-4">
                        <div class="form-group">
                            <input class="form-control" type="text" placeholder="Name" />
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <div class="form-group">
                            <input class="form-control" type="text" placeholder="Email" />
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <div class="form-group">
                            <input class="form-control" type="text" placeholder="Phone No." />
                        </div>
                    </div>
                    <div class="col-md-12">
                        <textarea class="form-control" placeholder="Message"></textarea>
                    </div>
                    <p>
                        <button class="btn btn-sm">Submit</button>
                    </p>
                </div>
            </div>
        </div>
    </div>
    <?php
        if($pageObject['31']['48']['background_image'] == 1) {
            $imgUrl = 'https://crm.prisu.nz/assets/backend/images/website/slide-block/'.$orgId.'/'.$pageObject['31']['48']['background_image_id'].'.'.$pageObject['31']['48']['background_image_ext'];
        }
        else {
            $imgUrl = 'plese set to default';
        }
    ?>
    <div class="section nobottommargin" style="background: url('{{ $imgUrl }}') center center no-repeat; background-size: cover;">
        <div class="container clear-bottommargin clearfix">
            <div class="row topmargin-sm clearfix section-4">
                <div class="col-md-8 col-md-offset-4">
                    <h4>Carpet and Flooring Calculator</h4>
                    <p>Try our clever calculator to estimate the cost of carpets and hard flooring</p>
                    <p>
                        <a href="{{URL::Route('price-cal')}}"> <button class="btn btn-sm">Click here to try</button></a>
                    </p>
                </div>
            </div>
        </div>
    </div>
@endsection
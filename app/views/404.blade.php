<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 11/02/18 15:54.
 */
?>

@extends('master')

@section('content')
    <div class="container" id="price-cal-container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <p style="text-align: center;">
                    <img src="{{URL::To('/')}}/assets/img/404.svg" style="width: 50%;" />
                </p>
            </div>
        </div>
    </div>
@endsection

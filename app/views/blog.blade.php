<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 2/02/18 08:59.
 */
?>

@extends('master')

@section('content')

    <div class="container clearfix" style="margin-bottom: 50px;">

        <!-- Posts
        ============================================= -->
        <div id="posts">

            <div class="entry clearfix">
                <div class="entry-image">
                    <a href="{{URL::To('/')}}/assets/img/blog/1.jpg" data-lightbox="image"><img class="image_fade" src="{{URL::To('/')}}/assets/img/blog/1.jpg" alt="Standard Post with Image"></a>
                </div>
                <div class="entry-title">
                    <h2><a href="blog-single.html">What is Bamboo Flooring</a></h2>
                </div>
                <ul class="entry-meta clearfix">
                    <li><i class="icon-calendar3"></i> 10th December 2017</li>
                    <li><a href="#"><i class="icon-user"></i> admin</a></li>
                    <li><a href="blog-single.html#comments"><i class="icon-comments"></i> 13 Comments</a></li>
                    <li><a href="#"><i class="icon-camera-retro"></i></a></li>
                </ul>
                <div class="entry-content">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate, asperiores quod est tenetur in. Eligendi, deserunt, blanditiis est quisquam doloribus voluptate id aperiam ea ipsum magni aut perspiciatis rem voluptatibus officia eos rerum deleniti quae nihil facilis repellat atque vitae voluptatem libero at eveniet veritatis ab facere.</p>
                    <a href="blog-single.html"class="more-link">Read More</a>
                </div>
            </div>

            <div class="entry clearfix">
                <div class="entry-image">
                    <video controls poster="{{URL::To('/')}}/assets/img/video/carpetlink_v1.jpg" style="width: 100%;">
                        <source src="{{URL::To('/')}}/assets/img/video/carpetlink_v1.mp4" type="video/mp4">
                        Your browser does not support HTML5 video.
                    </video>
                </div>
                <div class="entry-title">
                    <h2><a href="blog-single-full.html">Visit and experience by yourself</a></h2>
                </div>
                <ul class="entry-meta clearfix">
                    <li><i class="icon-calendar3"></i> 16th January 2018</li>
                    <li><a href="#"><i class="icon-user"></i> admin</a></li>
                    <li><i class="icon-folder-open"></i> <a href="#">Videos</a>, <a href="#">News</a></li>
                    <li><a href="blog-single-full.html#comments"><i class="icon-comments"></i> 19 Comments</a></li>
                    <li><a href="#"><i class="icon-film"></i></a></li>
                </ul>
                <div class="entry-content">
                    <p>Asperiores, tenetur, blanditiis, quaerat odit ex exercitationem pariatur quibusdam veritatis quisquam laboriosam esse beatae hic perferendis velit deserunt soluta iste repellendus officia in neque veniam debitis placeat quo unde reprehenderit eum facilis vitae. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nihil, reprehenderit!</p>
                    <a href="blog-single-full.html"class="more-link">Read More</a>
                </div>
            </div>

        </div><!-- #posts end -->

        <!-- Pagination
        ============================================= -->
        <ul class="pager nomargin">
            <li class="previous"><a href="#">&larr; Older</a></li>
            <li class="next"><a href="#">Newer &rarr;</a></li>
        </ul><!-- .pager end -->

    </div>

@endsection

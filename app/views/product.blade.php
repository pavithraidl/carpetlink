<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 9/02/18 12:40.
 */
?>

@extends('master')

@section('content')
    <div class="container clearfix">

        <div class="single-product">

            <div class="product">

                <div class="col-md-4">

                    <!-- Product Single - Gallery
                    ============================================= -->
                    <div class="product-image">
                        <div class="fslider" data-pagi="false" data-arrows="false" data-thumbs="true">
                            <div class="flexslider">
                                <div class="slider-wrap" data-lightbox="gallery">
                                    @if(sizeof($product['img']) >= 1)
                                    @foreach($product['img'] as $img)
                                        <?php
                                            $imgUrl = "https://crm.prisu.nz/assets/frontend/img/products/".$product['id']."/".$product['detailid']."/".$img['id'].".".$img['ext'];
                                            if($img['main'] == 1)
                                                $imgTitle = 'Main Image';
                                            else
                                                $imgTitle = 'Other Image';
                                        ?>
                                        <div class="slide" data-thumb=""><a href="{{ $imgUrl }}" title="{{ $product['name'] }} - {{ $imgTitle }}" data-lightbox="gallery-item"><img src="{{ $imgUrl }}" alt="{{ $product['name'] }} - Image"></a></div>
                                    @endforeach
                                    @else
                                        <?php $imgUrl = 'https://crm.prisu.nz/assets/frontend/img/products/default/400X400.png' ?>
                                        <div class="slide" data-thumb=""><a href="{{ $imgUrl }}" title="{{ $product['name'] }}" data-lightbox="gallery-item"><img style="opacity: 0.1;" src="{{ $imgUrl }}" alt="{{ $product['name'] }} - Image"></a></div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div><!-- Product Single - Gallery End -->

                </div>

                <div class="col-md-8 product-desc">

                    <!-- Product Single - Price
                    ============================================= -->
                    <div class="product-price">{{ $product['name'] }}</div><!-- Product Single - Price End -->

                    <!-- Product Single - Rating
                    ============================================= -->
                    <div class="product-rating">
                        <i class="icon-star3"></i>
                        <i class="icon-star3"></i>
                        <i class="icon-star3"></i>
                        <i class="icon-star-half-full"></i>
                        <i class="icon-star-empty"></i>
                    </div><!-- Product Single - Rating End -->

                    <div class="clear"></div>
                    <div class="line"></div>

                    <!-- Product Single - Quantity & Cart Button
                    ============================================= -->
                    <div class="col-md-12">
                        @if(sizeof($product['detail']) > 0)
                        @foreach($product['detail'] as $detail)
                            @if($detail['default'] == 1)
                                <h4>${{ $detail['price'] }} per {{ $detail['measure'] }}</h4>
                            @else
                                <span>RRP ${{ $detail['price'] }} {{ $detail['measure'] }}</span>
                            @endif
                        @endforeach
                        @endif
                            <br/><br/><br/>
                        <div class="additional-features">
                            {{ $product['additional_info'] }}
                        </div>

                    </div>
                    <div class="clear"></div>
                    <div class="line"></div>

                    <!-- Product Single - Short Description
                    ============================================= -->
                    <h5>DESCRIPTION</h5>
                    {{ $product['description'] }}

                    <div class="col-md-12">
                        <div class="col-md-6 col-sm-6">

                        </div>
                        <div class="col-md-6">
                            <button class="btn btn-sm">Get a free measure and quote</button>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="clear"></div><div class="line"></div>
    </div>
@endsection

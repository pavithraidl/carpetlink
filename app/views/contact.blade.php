<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 2/02/18 08:59.
 */
?>

@extends('master')

@section('content')

    <div class="container clearfix">
        <h3 id="contact-title">{{ $pageObject['35']['52']['top_title'] }}</h3>
        <div class="row">
            <div class="col-md-12">
                @foreach($masterObject['36'] as $contact)
                <div class="col-md-4">
                    <div class="map">
                        <h4>{{ $contact['store_location'] }}</h4>
                        <div class="map-container">
                            <iframe src="{{ $contact['google_map_url'] }}" width="100%" height="80%" frameborder="0" style="border:0" allowfullscreen></iframe>
                        </div>
                        <div class="icon-text-group">
                            <img src="{{URL::To('/')}}/assets/img/icons/shape.png" />
                            <span>{{ $contact['address'] }}</span>
                        </div>
                        <div class="icon-text-group">
                            <img src="{{URL::To('/')}}/assets/img/icons/fill-3.png" />
                            <span>{{ $contact['store_manager_name'] }}<br/><a href="tel:{{ $contact['store_manager_phone_number'] }}">{{ $contact['store_manager_phone_number'] }}</a></span>
                        </div>
                        <div class="icon-text-group">
                            <img src="{{URL::To('/')}}/assets/img/icons/email.png" />
                            <span><a href="mailto:{{ $contact['store_manager_email'] }}">{{ $contact['store_manager_email'] }}</a> </span>
                        </div>
                        <div class="open-hours">
                            <h5>Open Hours</h5>
                            {{ $contact['open_hours'] }}
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
        <div class="clear"></div><div class="line"></div>
    </div>

    <div class="contact-form">
        <div class="container">
            <h3>Contact Form</h3>
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="col-md-4 form-group">
                        <input type="text" class="form-control" placeholder="Name" />
                    </div>
                    <div class="col-md-4 form-group">
                        <input type="text" class="form-control" placeholder="Email" />
                    </div>
                    <div class="col-md-4 form-group">
                        <input type="text" class="form-control" placeholder="Phone" />
                    </div>
                    <div class="col-md-12 form-group">
                        <textarea class="form-control" placeholder="Message"></textarea>
                    </div>
                    <p>
                        <button>Submit</button>
                    </p>
                </div>
            </div>
        </div>
    </div>

@endsection

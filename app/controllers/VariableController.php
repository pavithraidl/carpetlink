<?php 
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - {10/01/18} {19:16}.
 */
 
class VariableController extends BaseController {

    const orgId = 6;
    const key = 'Z9SG0006';
    const secret = 'FeXS70v5QkbIN3X4OfaW4gAuc0EP196Elk9PM6QKCnsU0xVcl0uVX5q6a8ac';
    const server = 'https://crm.prisu.nz/';
    const stagingExtenstion = '';
    const version = '1.0';

    public function getVariable($name) {
        try {
            $domain = URL::to('/').'/'; //website domain

            //returning relevant variable
            switch ($name) {
                case "orgid":
                    return self::orgId;
                    break;
                case "key":
                        return self::key;
                        break;
                case "secret":
                        return self::secret;
                        break;
                case "server":
                        return self::server;
                        break;
                case "domain":
                    return $domain;
                    break;

                case "version":
                    return self::version;
                    break;

                case 'staging-extension':
                    return self::stagingExtenstion;
                    break;
            }

        } Catch(Exception $ex) {
            return 0;
        }
    }
}
<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function getHome()
	{
	    $masterObject = $this->getMasterTemplateData();

	    $ids = [27, 28, 29, 31];
	    $pageObject = $this->getCustomData($ids);

//	    dd($pageObject);

		return View::make('default', array(
		    'page' => 'home',
            'masterObject' => $masterObject,
            'pageObject' => $pageObject,
            'orgId' => VariableController::orgId
        ));
	}

	public function getCarpets() {

        $masterObject = $this->getMasterTemplateData();

        $ids = [30];
        $pageObject = $this->getCustomData($ids);

	    return View::make('carpets', array(
	        'page' => 'carpets',
            'masterObject' => $masterObject,
            'pageObject' => $pageObject,
            'orgId' => VariableController::orgId
        ));
    }

    public function getHard() {

        $masterObject = $this->getMasterTemplateData();

        $ids = [32];
        $pageObject = $this->getCustomData($ids);

	    return View::make('hard', array(
	        'page' => 'hard',
            'masterObject' => $masterObject,
            'pageObject' => $pageObject,
            'orgId' => VariableController::orgId
        ));
    }

    public function getFreeMeasure() {

        $masterObject = $this->getMasterTemplateData();

        $ids = [33];
        $pageObject = $this->getCustomData($ids);

	    return View::make('free-measure', array(
	        'page' => 'free-measure',
            'masterObject' => $masterObject,
            'pageObject' => $pageObject,
            'orgId' => VariableController::orgId
        ));
    }

    public function getPriceCal() {

        $masterObject = $this->getMasterTemplateData();

        $ids = [34];
        $pageObject = $this->getCustomData($ids);
//        $searchList = $this->getSearchProducts('new');

//        dd($searchList);

	    return View::make('price-cal', array(
	        'page' => 'price-cal',
            'masterObject' => $masterObject,
            'pageObject' => $pageObject,
            'orgId' => VariableController::orgId
        ));
    }

    public function getBlog() {

        $masterObject = $this->getMasterTemplateData();

	    return View::make('blog', array(
	        'page' => 'blog',
            'masterObject' => $masterObject,
            'orgId' => VariableController::orgId
        ));
    }

    public function getContact() {

        $masterObject = $this->getMasterTemplateData();

        $ids = [35];
        $pageObject = $this->getCustomData($ids);

	    return View::make('contact', array(
	        'page' => 'contact',
            'masterObject' => $masterObject,
            'pageObject' => $pageObject,
            'orgId' => VariableController::orgId
        ));
    }

    public function getCarpetCategory($categoryId) {

        $masterObject = $this->getMasterTemplateData();
        $category = $this->getCategory($categoryId);

        return View::make('product-list', array(
            'page' => 'carpets',
            'masterObject' => $masterObject,
            'category' => $category,
            'orgId' => VariableController::orgId
        ));
    }

    public function getHardCategories($categoryId) {

        $masterObject = $this->getMasterTemplateData();
        $category = $this->getCategory($categoryId);

        return View::make('product-list', array(
            'page' => 'hard',
            'masterObject' => $masterObject,
            'category' => $category,
            'orgId' => VariableController::orgId
        ));
    }

    public function getCarpetProduct($productId) {
        $masterObject = $this->getMasterTemplateData();
        $product = $this->getProductDetails($productId);


        return View::make('product', array(
            'page' => 'product',
            'masterObject' => $masterObject,
            'product' => $product,
            'orgId' => VariableController::orgId
        ));
    }

    public function getMasterTemplateData() {
	    $sliderIds = [25, 26, 36];
        //API Url
        $url = 'https://crm.prisu.nz/website/getsliders';
        $ch = curl_init($url);

        //The JSON data.

        $jsonData = array(
            'orgid' => VariableController::orgId,
            'orgkey' => VariableController::key,
            'orgsecret' => VariableController::secret,
            'sliderids' => $sliderIds
        );

        //Encode the array into JSON.
        $jsonDataEncoded = json_encode($jsonData);

        //Tell cURL that we want to send a POST request.
        curl_setopt($ch, CURLOPT_POST, 1);

        //Attach our encoded JSON string to the POST fields.
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);

        //Set the content type to application/json
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));

        //Execute the request
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        $result = json_decode($result, true);

        return $result;
    }

    public function getCustomData($ids) {
        $sliderIds = $ids;
        //API Url
        $url = 'https://crm.prisu.nz/website/getsliders';
        $ch = curl_init($url);

        //The JSON data.

        $jsonData = array(
            'orgid' => VariableController::orgId,
            'orgkey' => VariableController::key,
            'orgsecret' => VariableController::secret,
            'sliderids' => $sliderIds
        );

        //Encode the array into JSON.
        $jsonDataEncoded = json_encode($jsonData);

        //Tell cURL that we want to send a POST request.
        curl_setopt($ch, CURLOPT_POST, 1);

        //Attach our encoded JSON string to the POST fields.
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);

        //Set the content type to application/json
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));

        //Execute the request
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        $result = json_decode($result, true);

        return $result;
    }

    public function getCategory($categoryId) {
        $url = 'https://crm.prisu.nz/products/getcategorydetails';
        $ch = curl_init($url);

        //The JSON data.

        $jsonData = array(
            'orgid' => VariableController::orgId,
            'orgkey' => VariableController::key,
            'orgsecret' => VariableController::secret,
            'categoryid' => $categoryId
        );

        //Encode the array into JSON.
        $jsonDataEncoded = json_encode($jsonData);

        //Tell cURL that we want to send a POST request.
        curl_setopt($ch, CURLOPT_POST, 1);

        //Attach our encoded JSON string to the POST fields.
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);

        //Set the content type to application/json
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));

        //Execute the request
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        $result = json_decode($result, true);

        return $result;
    }

    public function getProductDetails($productId) {
        $url = 'https://crm.prisu.nz/products/getproductdetails';
        $ch = curl_init($url);
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        //The JSON data.

        $jsonData = array(
            'orgid' => VariableController::orgId,
            'orgkey' => VariableController::key,
            'orgsecret' => VariableController::secret,
            'productid' => $productId,
            'userip' => $ip
        );

        //Encode the array into JSON.
        $jsonDataEncoded = json_encode($jsonData);

        //Tell cURL that we want to send a POST request.
        curl_setopt($ch, CURLOPT_POST, 1);

        //Attach our encoded JSON string to the POST fields.
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);

        //Set the content type to application/json
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));

        //Execute the request
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        $result = json_decode($result, true);

        return $result;
    }

    public function getSearchProducts($keyword) {
        $url = 'https://crm.prisu.nz/products/searchproducts';
        $ch = curl_init($url);

        //The JSON data.

        $jsonData = array(
            'orgid' => VariableController::orgId,
            'orgkey' => VariableController::key,
            'orgsecret' => VariableController::secret,
            'keyword' => $keyword,
            'paginate' => 1,
            'perpage' => 100,
        );

        //Encode the array into JSON.
        $jsonDataEncoded = json_encode($jsonData);

        //Tell cURL that we want to send a POST request.
        curl_setopt($ch, CURLOPT_POST, 1);

        //Attach our encoded JSON string to the POST fields.
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);

        //Set the content type to application/json
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));

        //Execute the request
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        $result = json_decode($result, true);

        return $result;
    }

}

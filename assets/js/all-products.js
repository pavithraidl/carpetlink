/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 7/02/18 19:15.
 */

"use strict";

window.categoryId = $('#category-id').val();
getCategoryProducts();

//Public API [ID: 304]
function getCategoryProducts() {
    //develop the function here
    console.log('run');
    //ajax method
    jQuery.ajax({
        type: 'POST',
        url: window.server + 'products/getcategoryproducts',
        data: {categoryid:window.categoryId, paginate:1, perpage:100, orgkey:window.orgKey, orgid:window.orgId, orgsecret:orgSecret}, //add the parameter if required to pass
        success: function (data) {
            if(data == 'Unauthorized') {
                window.location.reload();
            }
            else if(data == 0) {
                dbError();
            }
            else {
                var res = JSON.parse(data);
                var appendHtml = '';

                for(var key in res['category']) {

                    var productsHtml = '';

                    if(res['products'] != 'undefined') {
                        for(var i = 0; i < res['products'].length; i++) {
                            if(res['products'][i].sub_category_id == res['category'][key].id) {

                                var imgUrl = 'assets/frontend/img/products/default/400X400.png';
                                var defaultImg = 'default';
                                if(res['products'][i].img_id != null) {
                                    imgUrl = 'assets/frontend/img/products/'+res['products'][i].id+'/'+res['products'][i].detail_id+'/'+res['products'][i].img_id+'.'+res['products'][i].img_ext;
                                    defaultImg = '';
                                }

                                productsHtml += '<div class="col-md-4 col-sm-6">' +
                                    '               <a href="'+window.domain+'product/'+res['products'][i].id+'">' +
                                    '                   <div id="product-'+res['products'][i].id+'" class="product">' +
                                    '                       <div class="img-container">' +
                                    '                           <img class="'+defaultImg+'" src="'+window.server+imgUrl+'" />' +
                                    '                       </div>' +
                                    '                       <h4>'+res['products'][i].name+'</h4>' +
                                    '                       <h5>$'+res['products'][i].price+' / '+res['products'][i].measure+'</h5>' +
                                    '                   </div>' +
                                    '               </a>' +
                                    '           </div>'
                            }
                        }
                    }


                    appendHtml += ' <div id="sub-section-'+res['category'][key].id+'" class="product-list row">' +
                        '               <h4>'+res['category'][key].name+'</h4>' +
                        '               <p>'+res['category'][key].description+'</p>'
                        +productsHtml+
                        '           </div>'
                }

                productsHtml = '';
                if(res['products']) {
                    for(var i = 0; i < res['products'].length; i++) {

                        if(res['products'][i].sub_category_id == null) {
                            var imgUrl = 'assets/frontend/img/products/default/400X400.png';
                            var defaultImg = 'default';
                            if(res['products'][i].img_id != null) {
                                imgUrl = 'assets/frontend/img/products/'+res['products'][i].id+'/'+res['products'][i].detail_id+'/'+res['products'][i].img_id+'.'+res['products'][i].img_ext;
                                defaultImg = '';
                            }

                            productsHtml += '<div class="col-md-4 col-sm-6">' +
                                '               <a href="'+window.domain+'product/'+res['products'][i].id+'">' +
                                '                   <div id="product-'+res['products'][i].id+'" class="product">' +
                                '                       <div class="img-container">' +
                                '                           <img class="'+defaultImg+'" src="'+window.server+imgUrl+'" />' +
                                '                       </div>' +
                                '                       <h4>'+res['products'][i].name+'</h4>' +
                                '                       <h5>$'+res['products'][i].price+' / '+res['products'][i].measure+'</h5>' +
                                '                   </div>' +
                                '               </a>' +
                                '           </div>'
                        }

                    }
                }

                if(productsHtml == '') {
                    appendHtml = '<p style="text-align: center; margin-top: 50px;"> No Products found under this category!</p>';
                }
                else {
                    appendHtml = appendHtml + '<div id="sub-section-other" class="product-list row"><h4>Others</h4>'+productsHtml+'</div>';
                }


                $('#product-list-container').html(appendHtml);
            }
        },
        error: function (xhr, textShort, errorThrown) {
            noConnection();
            unBlockUI(el);
        }
    });
}
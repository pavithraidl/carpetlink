/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 6/02/18 00:25.
 */

"use strict";
getMenu();


function getMenu() {
    //develop the function here
    var menuIds = [11];

    var el = $('#hard-category-container');
    blockUI(el);
    //ajax method
    jQuery.ajax({
        type: 'GET',
        url: window.server + 'products/getmenu',
        data: {menuids:menuIds, orgkey:window.orgKey, orgid:window.orgId, orgsecret:orgSecret}, //add the parameter if required to pass
        success: function (data) {
            if(data == 'Unauthorized') {
                window.location.reload();
            }
            else if(data == 0) {
                dbError();
            }
            else {
                var res = JSON.parse(data);
                var appendHtml = '';

                for(var i = 0; i < res[11].length; i++) {
                    appendHtml += '<div class="col-md-4 col-sm-6 col-xs-12">' +
                        '               <a href="'+window.domain+'hard-flooring/category/'+res[11][i].id+'">' +
                        '                   <div class="category">' +
                        '                       <div class="img-container">' +
                        '                           <img src="'+window.server+'assets/backend/images/products/categories/'+window.orgId+'/'+res[11][i].id+'.jpg" />' +
                        '                           <div class="overlay">' +
                        '                               <p>'+res[11][i].description+'</p>' +
                        '                           </div>' +
                        '                       </div>' +
                        '                       <h4>'+res[11][i].name+'</h4>' +
                        '                   </div>' +
                        '               </a>' +
                        '           </div>';
                }

                $('#hard-category-container').html(appendHtml);
                unBlockUI(el);

            }
        },
        error: function (xhr, textShort, errorThrown) {
            noConnection();
            unBlockUI(el);
        }
    });
}
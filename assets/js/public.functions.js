/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 7/02/18 19:22.
 */

"use strict";

function blockUI(object) {
    if(object.find('.widget-loading').length != 1) {
        object.addClass('widget-loading');
        object.fadeTo(300, 0.5).find("*").attr("disabled", "disabled");
        var loadingHtml = '<object id="widget-loading-icon" class="widget-loading-icon" data="'+window.server+'assets/backend/images/loading/ring-sm.svg" style="z-index: 100; position: absolute; top: 48%; left: 48%;" type="image/svg+xml"></object>';
        object.parent().append(loadingHtml);
    }
}

function unBlockUI(object) {
    $('#widget-loading-icon').fadeOut(200);
    setTimeout(function () {
        $('#widget-loading-icon').remove();
    });
    object.removeClass('widget-loading');
    object.fadeTo(200, 1).find("*").removeAttr("disabled");
}

$('.only-digits').bind('keypress', function (e) {
    if (e.keyCode < 48 || e.keyCode > 57) {
        if(e.keyCode == 8 || e.keyCode == 46 || e.keyCode == 9 || e.keyCode == 37 || e.keyCode == 38 || e.keyCode == 46 || e.keyCode == 40 || e.keyCode == 39) {

        }
        else {
            return false;
        }
    }
});

function maxVal(object, max) {
    var id = $('#'+object.id);
    var value = id.val();
    if(value >= max) {
        return false;
    }
}

$('#search-form').on('keyup keypress', function(e) {
    var keyCode = e.keyCode || e.which;
    if (keyCode === 13) {
        e.preventDefault();
        return false;
    }
});

$('#search-keyword').bind('keyup', function (e) {
    var keyword = $.trim($('#search-keyword').val());

    if(e.keyCode == 32 || e.keyCode == 13) {
        searchProduct(keyword);
    }
    else if(keyword == '') {
        clearProductSearch();
    }
});

$('.search-dialog').slideUp();

function searchProduct(keyword) {
    var el = $('.search-dialog');
    blockUI(el);
    el.slideDown(200);

    jQuery.ajax({
        type: 'POST',
        url: window.server + 'products/searchproducts',
        data: {paginate:1, perpage:100, orgkey:window.orgKey, orgid:window.orgId, orgsecret:orgSecret, keyword:keyword}, //add the parameter if required to pass
        success: function (data) {
            if(data == 'Unauthorized') {
                window.location.reload();
            }
            else if(data == 0) {
                dbError();
            }
            else {
                var res = JSON.parse(data);
                var appendHtml = '';

                for(var i = 0; i < res.length; i++) {

                    var imgUrl = 'assets/frontend/img/products/default/400X400.png';
                    var defaultImg = 'opacity:0.1;';
                    if(res[i].img != null) {
                        if(res[i].img.id != null) {
                            imgUrl = 'assets/frontend/img/products/'+res[i].id+'/'+res[i].detail_id+'/'+res[i].img.id+'.'+res[i].img.ext;
                            defaultImg = '';
                        }
                    }

                    appendHtml += ' <div class="col-md-3 col-sm-4 col-xs-12">' +
                        '               <a href="'+window.domain+'product/'+res[i].id+'">' +
                        '                   <div class="product" style="padding: 15px;">' +
                        '                       <div class="img-container">' +
                        '                           <img style="'+defaultImg+'" class="'+defaultImg+'" src="'+window.server+imgUrl+'" />' +
                        '                       </div>' +
                        '                       <h4 style="font-size: 12px;" class="product-name">'+res[i].name+'</h4>' +
                        '                       <h5>$'+res[i].price+' / '+res[i].measure+'</h5>' +
                        '                   </div>' +
                        '               </a>' +
                        '           </div>';
                }

                if(appendHtml == '') {
                    appendHtml = '<p style="text-align: center; margin-top: 20px; color: #c5c5c5;">Sorry...! No Products Found.</p>'
                }

                $('#product-search-result-container').html(appendHtml);
                unBlockUI(el);
            }
        },
        error: function (xhr, textShort, errorThrown) {
            noConnection();
            unBlockUI(el);
        }
    });
}

function clearProductSearch() {
    $('#product-search-result-container').html('');
    $('.search-dialog').slideUp(200);
}
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 10/02/18 09:29.
 */

"use strict";

//region Global Variables
$('#search-result-container').slideUp();
$('#search-loading-icon').fadeOut();
//endregion

//region Default settings

//endregion

//region Events

//endregion

//region Functions
$('#keyword').bind('keyup', function (e) {
    var keyword = $.trim($('#keyword').val());
    if(e.keyCode == 32 || e.keyCode == 13) {
        searchProducts(keyword);
    }
    else if(keyword == '') {
        clearSearch();
    }
});

function searchProducts(keyword) {
    //develop the function here

    $('#search-result-container').slideDown(250);
    $('#search-loading-icon').fadeIn(200);

    //ajax method
    jQuery.ajax({
        type: 'POST',
        url: window.server + 'products/searchproducts',
        data: {paginate:1, perpage:100, orgkey:window.orgKey, orgid:window.orgId, orgsecret:orgSecret, keyword:keyword}, //add the parameter if required to pass
        success: function (data) {
            if(data == 'Unauthorized') {
                window.location.reload();
            }
            else if(data == 0) {
                dbError();
            }
            else {
                var res = JSON.parse(data);
                var appendHtml = '';

                for(var i = 0; i < res.length; i++) {
                    appendHtml += '<li id="slider'+res[i].id+'" onclick="setupPrice('+res[i].id+', \''+res[i].price+'\', \''+res[i].name+'\');"><name>'+res[i].name+'</name> <price>$'+res[i].price+'/'+res[i].measure+'</price></li>';
                }

                if(appendHtml == '') {
                    appendHtml = '<p style="text-align: center; margin-top: 20px; color: #c5c5c5;">Sorry...! No Products Found.</p>'
                }

                $('#search-result-ul').html(appendHtml);
                $('#search-loading-icon').fadeOut(200);
            }
        },
        error: function (xhr, textShort, errorThrown) {
            noConnection();
            unBlockUI(el);
        }
    });
}

function clearSearch() {
    $('#search-result-ul').html('');
    $('#search-loading-icon').fadeOut(200);
    $('#search-result-container').slideUp(250);
}

function setupPrice(productId, price, name) {
    clearSearch();
    $('#keyword').val(name);
    $('#price').val(price);
    calculatePrice();
}

function calculatePrice() {
    var price = $('#price').val();
    var length = $('#input-length').val();
    var long = $('#input-long').val();

    if(price != '' && length != '' && long != '') {
        var error = 0;
        if(isNaN(price)) {
            $('#keyword').effect( "shake" , 300);
            error = 1;
        }
        else if(isNaN(length)) {
            $('#input-length').effect( "shake" , 300);
            error = 1;
        }
        else if(isNaN(long)) {
            $('#input-long').effect( "shake" , 300);
            error = 1;
        }

        if(error == 0) {
            var totalArea = length*long;
            var cost = parseFloat(totalArea*price);

            $('#total-area-display').html(totalArea.toFixed(2));
            $('#cost').html('$ '+cost.toFixed(2));
        }
    }
}
//endregion
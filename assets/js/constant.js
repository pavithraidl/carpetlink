/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 5/02/18 23:45.
 */

"use strict";

window.domain = $('#org-domain').val();
window.server = $('#org-server').val();
window.orgId = $('#org-id').val();
window.orgKey = $('#org-key').val();
window.orgSecret = $('#org-secret').val();
